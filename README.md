# river-list-tags

_This is a fork of https://github.com/tarneaux/river-get-tags_

Get focused tag number.

# Installation

```sh
make
sudo make install
```

# Usage

Just run the tool:
```sh
river-list-tags
```

# Credits & License

This tool is heavily based on [Adithya Kumar](https://gitlab.com/akumar-xyz)'s [river-shifttags](https://gitlab.com/akumar-xyz/river-shifttags). I basically just removed some of the functionality and adjusted the prints.

License: GPL version 3
